Feature: zippopotam API Tests

  Background:
    * url 'http://api.zippopotam.us'

  Scenario: API Test for http://api.zippopotam.us/de/bw/stuttgart
    Given path '/de/bw/stuttgart'
    When method GET
    Then status 200
    And assert responseTime < 1000
    And match responseType == 'json'
    And match $.country == 'Germany'
    And match $.state == 'Baden-Württemberg'
    * def placeNames = $.places[?(@.['post code'] == '70597')].['place name']
    And match placeNames contains 'Stuttgart Degerloch'

  Scenario Outline: API Data Driven Test for http://api.zippopotam.us/{country}/{postal-code}
    Given path '/<country>/<postalCode>'
    When method GET
    Then status 200
    And assert responseTime < 1000
    And match responseType == 'json'
    * def placeNames = $.places..['place name']
    And match placeNames contains '<placeName>'

    Examples:
      | country | postalCode | placeName     |
      | us      | 90210      | Beverly Hills |
      | us      | 12345      | Schenectady   |
      | ca      | B2R        | Waverley      |