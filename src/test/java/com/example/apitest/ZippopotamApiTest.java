package com.example.apitest;

import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.ResponseSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static io.restassured.RestAssured.get;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.Matchers.*;

class ZippopotamApiTest {

  private ResponseSpecification responseSpec;

  @BeforeEach
  void setup() {
    RestAssured.baseURI = "http://api.zippopotam.us";

    responseSpec = new ResponseSpecBuilder()
      .expectStatusCode(200)
      .expectContentType("application/json")
      .expectResponseTime(lessThan(1L), SECONDS)
      .build();
  }

  @Test
  void zippopotamDeBwStuttgart() {
    get("/de/bw/stuttgart").
      then()
      .spec(responseSpec)
      .body("country", is("Germany"))
      .body("state", is("Baden-Württemberg"))
      .body("places.findAll{ it.'post code' == '70597'}.'place name'", hasItem("Stuttgart Degerloch"));
  }


  @ParameterizedTest
  @CsvSource({
    "us, 90210, Beverly Hills",
    "us, 12345, Schenectady",
    "ca, B2R,   Waverley"
  })
  void zippopotamDataDriven(String country, String postalCode, String placeName) {
    get("/{country}/{postalCode}", country, postalCode).
      then()
      .spec(responseSpec)
      .body("places.'place name'", hasItem(placeName));
  }
}
